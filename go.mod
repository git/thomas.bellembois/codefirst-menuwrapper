module codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-menuwrapper

go 1.19

require (
	codefirst.iut.uca.fr/git/thomas.bellembois/codefirst-menu/v2 v2.0.0-20230117110115-2fa6f8c3ab7d
	github.com/sirupsen/logrus v1.9.0
)

require golang.org/x/sys v0.4.0 // indirect
