#!/usr/bin/env bash

debug=""

if [ ! -z "$DEBUG" ]
then
    debug="-debug"
fi

/go/bin/codefirst-menuwrapper -proxyurl=$PROXYURL -codefirsturl=$CODEFIRSTURL $debug