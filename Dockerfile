FROM golang:1.19-bullseye
LABEL author="Thomas Bellembois"

# Copying sources.
WORKDIR /go/src/github.com/tbellembois/codefirst-menuwrapper/
COPY . .

# Installing.
RUN go install -v ./...
RUN chmod +x /go/bin/codefirst-menuwrapper

# Copying entrypoint.
COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh

USER www-data
EXPOSE 8081
CMD ["/entrypoint.sh"]